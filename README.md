YutooAI Grpc SDK Demo for C++
===================

由图AI Grpc SDK 演示代码 C++ 版本

**使用此命令克隆**

`git clone --recurse-submodules -j8`

## 依赖

**基础依赖**
- `grpc`
- `protobuf`

**Qt依赖**
- `qt` >= 5.15 或 `mingw` >= 8.1.0

## 构建

### 方法一 **【推荐】**
- [基于vcpkg的构建]()

### 方法二
- [基于msys2的构建]()