﻿#include "yutooaisdk.h"
#include "yutooai.hpp"

#include <memory>
#include <string>

inline StatusCode return_msg(REPLY_STATE &replyState, const Status &grpcState, const PublicReply &reply) {
    if (grpcState.ok()) {
        memcpy(&replyState.message[0], reply.msg().c_str(),
               reply.msg().length() >= sizeof(replyState.message) ? sizeof(replyState.message) - 1
                                                                  : reply.msg().length());
        replyState.code = (YUTOO_SDK_REPLY_CODE)reply.code();
    } else {
        memcpy(&replyState.message[0], grpcState.error_message().c_str(),
               grpcState.error_message().length() >= sizeof(replyState.message) ? sizeof(replyState.message) - 1
                                                                                : grpcState.error_message().length());
        replyState.code = YUTOO_SDK_REPLY_CODE::ErrUnknown;
    }
    return grpcState.error_code();
}

std::unique_ptr<YutooAiClient> yutooAi;

void YutooAI_SDK_Connect(const char *addr) { yutooAi = std::make_unique<YutooAiClient>(addr); }

void YutooAI_SDK_Disconnect() {
    REPLY_STATE state;
    YutooAI_SDK_UnInit(state);
    yutooAi.reset();
}

GRPC_CHANNEL_STATE YutooAI_SDK_Status() {
    if (yutooAi) {
        return (GRPC_CHANNEL_STATE)yutooAi->GetChannelStatus(true);
    }
    return GRPC_CHANNEL_STATE::CHANNEL_SHUTDOWN;
}

int YutooAI_SDK_Init(REPLY_STATE &state) {
    if (yutooAi) {
        PublicReply reply;

        auto grpcState = yutooAi->Init(reply);
        return return_msg(state, grpcState, reply);
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_UnInit(REPLY_STATE &state) {
    if (yutooAi) {
        PublicReply reply;

        auto grpcState = yutooAi->UnInit(reply);
        return return_msg(state, grpcState, reply);
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_Activate(const char *key, const char *shopName, const char *contacterName, const char *contacterPhone,
                         REPLY_STATE &state) {
    if (yutooAi) {
        PublicReply     reply;
        ActivateRequest req;

        auto actInfo = req.mutable_activateinfo();
        actInfo->set_cdkey(key);
        auto shopInfo = actInfo->mutable_shopinfo();
        shopInfo->set_name(shopName);
        shopInfo->set_contact(contacterName);
        shopInfo->set_phone(contacterPhone);

        auto grpcState = yutooAi->Activate(req, reply);
        return return_msg(state, grpcState, reply);
    }
    return StatusCode::UNAVAILABLE;
}

std::mutex     pluSyncMtx;
PluSyncRequest pluListRequest;

void YutooAI_SDK_PluAdd(const char *plucode, const char *pluname, bool sale, bool lock) {
    std::unique_lock<std::mutex> locker(pluSyncMtx);

    auto newItem = pluListRequest.add_plulist();
    auto plu     = newItem->mutable_plu();
    plu->set_code(plucode);
    plu->set_name(pluname);
    newItem->set_issale(sale);
    newItem->set_islock(lock);
}

int YutooAI_SDK_PluSync(REPLY_STATE &state) {
    std::unique_lock<std::mutex> locker(pluSyncMtx);

    auto ret = StatusCode::UNAVAILABLE;
    if (yutooAi) {
        PublicReply reply;

        pluListRequest.set_isdemo(false);

        auto grpcState = yutooAi->PluSync(pluListRequest, reply);
        ret            = return_msg(state, grpcState, reply);
    }
    pluListRequest.clear_plulist();
    return ret;
}

std::mutex             pluStatMtx;
UpdatePluStatusRequest pluStatRequest;

void YutooAI_SDK_UpdatePluAdd(const char *plucode, const char *pluname, bool sale, bool lock) {
    std::unique_lock<std::mutex> locker(pluStatMtx);

    auto status = pluStatRequest.add_plustatuslist();
    auto plu    = status->mutable_plu();
    plu->set_code(plucode);
    plu->set_name(pluname);
    status->set_issale(sale);
    status->set_islock(lock);
}

int YutooAI_SDK_UpdatePluStatus(REPLY_STATE &state) {
    std::unique_lock<std::mutex> locker(pluStatMtx);

    auto ret = StatusCode::UNAVAILABLE;
    if (yutooAi) {
        PublicReply reply;

        auto grpcState = yutooAi->UpdatePluStatus(pluStatRequest, reply);
        ret            = return_msg(state, grpcState, reply);
    }
    pluStatRequest.clear_plustatuslist();
    return ret;
}

int YutooAI_SDK_ResetDemo(REPLY_STATE &state) {
    if (yutooAi) {
        PublicReply    reply;
        PluSyncRequest req;
        req.set_isdemo(true);
        auto newitem = req.add_plulist();
        auto plu     = newitem->mutable_plu();
        plu->set_code("1000");
        plu->set_name("Demo");
        newitem->set_islock(true);
        newitem->set_issale(false);

        auto grpcState = yutooAi->PluSync(req, reply);
        return return_msg(state, grpcState, reply);
    }
    return StatusCode::UNAVAILABLE;
}

std::mutex matchMtx;

int YutooAI_SDK_AiMatching(int maxNum, YutooAI_SDK_AiMatching_CallBack cb) {
    std::unique_lock<std::mutex> locker(matchMtx);

    if (yutooAi) {
        AiMatchingReply   reply;
        AiMatchingRequest req;

        req.set_weight(999);
        req.set_stable(WeightStatusType::Stable);
        req.set_maxnum(maxNum);
        req.set_matchtype(MatchingType::Forcibly);

        auto grpcState = yutooAi->AiMatching(req, reply);
        if (grpcState.ok()) {
            REPLY_STATE state;
            auto        retCode = return_msg(state, grpcState, reply.status());
            if (cb) {
                auto resultList = std::vector<RESULT_INFO>();
                resultList.reserve(reply.plulist_size());
                for (auto &it : reply.plulist()) {
                    resultList.push_back({it.code().c_str(), it.name().c_str()});
                }
                cb(state, reply.matchindex(), reply.identifyid(), resultList.size(), resultList.data());
            }
            return retCode;
        }
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_AiMark(const long long identifyid, const char *plucode, bool isSearch, REPLY_STATE &state) {
    std::unique_lock<std::mutex> locker(matchMtx);

    if (yutooAi) {
        PublicReply   reply;
        AiMarkRequest req;

        req.set_identifyid(identifyid);
        req.set_marktype(isSearch ? MarkType::Search : MarkType::MatchOut);
        auto plu = req.mutable_plu();
        plu->set_code(plucode);
        auto plusaleinfo = req.mutable_plusaleinfo();
        plusaleinfo->set_sales(99);
        plusaleinfo->set_priceunit(PriceUnitType::Weight);
        plusaleinfo->set_price(99);
        plusaleinfo->set_saleprice(99);
        plusaleinfo->set_amount(999);
        plusaleinfo->set_barcode(plucode);

        auto grpcState = yutooAi->AiMark(req, reply);
        return return_msg(state, grpcState, reply);
    }
    return StatusCode::UNAVAILABLE;
}

std::mutex           studyMtx;
StudyModeMarkRequest studyListRequest;

int YutooAI_SDK_StudyMatching(YutooAI_SDK_Image_CallBack cb) {
    std::unique_lock<std::mutex> locker(studyMtx);

    if (yutooAi) {
        StudyModeMatchingReply reply;

        auto grpcState = yutooAi->StudyModeMatching(reply);
        if (grpcState.ok()) {
            studyListRequest.add_identifyidlist(reply.identifyid());
            locker.unlock();

            REPLY_STATE state;
            auto        retCode = return_msg(state, grpcState, reply.status());
            if (cb) {
                auto &img = reply.imgdata();
                cb(state, (const unsigned char *)img.data(), img.length());
            }
            return retCode;
        }
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_StudyMark(REPLY_STATE &state) {
    std::unique_lock<std::mutex> locker(studyMtx);

    auto ret = StatusCode::UNAVAILABLE;
    if (yutooAi) {
        PublicReply reply;

        auto plusaleinfo = studyListRequest.mutable_plusaleinfo();
        plusaleinfo->set_sales(99);
        plusaleinfo->set_priceunit(PriceUnitType::Weight);
        plusaleinfo->set_price(99);
        plusaleinfo->set_saleprice(99);
        plusaleinfo->set_amount(999);
        plusaleinfo->set_barcode("0");

        auto grpcState = yutooAi->StudyModelMark(studyListRequest, reply);
        ret            = return_msg(state, grpcState, reply);
    }
    studyListRequest.clear_identifyidlist();
    return ret;
}

std::vector<CAMERA_INFO> camera_info_list;

int YutooAI_SDK_GetCameraList(YutooAI_SDK_CameraList_CallBack cb) {
    if (yutooAi) {
        GetCameraListReply reply;

        auto grpcState = yutooAi->GetCameraList(reply);
        if (grpcState.ok()) {
            REPLY_STATE state;
            auto        retCode = return_msg(state, grpcState, reply.status());
            if (cb) {
                auto  selected = reply.selectedcamera().id();
                auto &camlist  = reply.cameralist();

                camera_info_list.clear();
                for (auto &cam : camlist) {
                    camera_info_list.push_back(
                        {cam.id(), cam.name().c_str(), cam.symboliclink().c_str(), cam.locationinfo().c_str()});
                }

                cb(state, selected, camera_info_list.size(), camera_info_list.data());
            }
            return retCode;
        }
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_OpenCamera(unsigned int id, REPLY_STATE &state) {
    if (yutooAi) {
        PublicReply       reply;
        OpenCameraRequest req;

        auto selected = req.mutable_selectedcamera();
        selected->set_id(id);

        auto grpcState = yutooAi->OpenCamera(req, reply);

        return return_msg(state, grpcState, reply);
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_GetCameraImage(YutooAI_SDK_Image_CallBack cb) {
    if (yutooAi) {
        GetCameraStreamReply reply;

        auto grpcState = yutooAi->GetCameraImage(reply);
        if (grpcState.ok()) {
            REPLY_STATE state;
            auto        retCode = return_msg(state, grpcState, reply.status());
            if (cb) {
                auto &img = reply.imgdata();
                cb(state, (const unsigned char *)img.data(), img.length());
            }
            return retCode;
        }
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_GetCameraArea(AREA_INFO &area, REPLY_STATE &state) {
    if (yutooAi) {
        GetCameraAreaReply reply;

        auto grpcState = yutooAi->GetCameraArea(reply);
        if (grpcState.ok()) {
            auto areaInfo      = reply.mutable_cameraareainfo();
            area.leftTop_x     = areaInfo->lefttopx();
            area.leftTop_y     = areaInfo->lefttopy();
            area.rightTop_x    = areaInfo->righttopx();
            area.rightTop_y    = areaInfo->righttopy();
            area.rightBottom_x = areaInfo->rightbottomx();
            area.rightBottom_y = areaInfo->rightbottomy();
            area.leftBottom_x  = areaInfo->leftbottomx();
            area.leftBottom_y  = areaInfo->leftbottomy();
        }
        return return_msg(state, grpcState, reply.status());
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_SetCameraArea(AREA_INFO *area, REPLY_STATE &state) {
    if (yutooAi) {
        PublicReply          reply;
        SetCameraAreaRequest req;

        auto areaInfo = req.mutable_cameraareainfo();
        areaInfo->set_lefttopx(area->leftTop_x);
        areaInfo->set_lefttopy(area->leftTop_y);
        areaInfo->set_righttopx(area->rightTop_x);
        areaInfo->set_righttopy(area->rightTop_y);
        areaInfo->set_rightbottomx(area->rightBottom_x);
        areaInfo->set_rightbottomy(area->rightBottom_y);
        areaInfo->set_leftbottomx(area->leftBottom_x);
        areaInfo->set_leftbottomy(area->leftBottom_y);

        auto grpcState = yutooAi->SetCameraArea(req, reply);
        return return_msg(state, grpcState, reply);
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_ShowCameraAreaView(REPLY_STATE &state) {
    if (yutooAi) {
        PublicReply reply;

        auto grpcState = yutooAi->ShowCameraAreaView(reply);
        return return_msg(state, grpcState, reply);
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_GetStudyData(YutooAI_SDK_Bytes_CallBack cb) {
    if (yutooAi) {
        GetStudyDataReply reply;

        auto grpcState = yutooAi->GetStudyData(reply);
        if (grpcState.ok()) {
            REPLY_STATE state;
            auto        retCode = return_msg(state, grpcState, reply.status());
            if (cb) {
                auto &data = reply.studydata();
                cb(state, (const unsigned char *)data.data(), data.length(), reply.studydatamd5().c_str());
            }
            return retCode;
        }
    }
    return StatusCode::UNAVAILABLE;
}

int YutooAI_SDK_SetStudyData(const unsigned char *bytes, const int &size, const char *md5, bool merge,
                             REPLY_STATE &state) {
    if (yutooAi) {
        if (bytes && size > 0) {
            PublicReply         reply;
            SetStudyDataRequest req;

            req.set_mode(merge ? SetStudyModeType::MERGER : SetStudyModeType::REPLACE);
            req.set_studydata(bytes, size);
            req.set_studydatamd5(md5 ? md5 : "");

            auto grpcState = yutooAi->SetStudyData(req, reply);
            return return_msg(state, grpcState, reply);
        }
    }
    return StatusCode::UNAVAILABLE;
}

std::string sdkVer;
std::string cameraVer;
std::string identifyVer;
std::string matchingVer;
std::string machineVer;

int YutooAI_SDK_Version(VERSION_INFO &ver, REPLY_STATE &state) {
    if (yutooAi) {
        GetVersionReply reply;

        auto grpcState = yutooAi->GetVersion(reply);
        if (grpcState.ok()) {
            sdkVer      = reply.sdkversion();
            cameraVer   = reply.cameraversion();
            identifyVer = reply.identifyversion();
            matchingVer = reply.matchingversion();
            machineVer  = reply.machineversion();

            ver = {sdkVer.c_str(), cameraVer.c_str(), identifyVer.c_str(), matchingVer.c_str(), machineVer.c_str()};
        }
        return return_msg(state, grpcState, reply.status());
    }
    return StatusCode::UNAVAILABLE;
}
