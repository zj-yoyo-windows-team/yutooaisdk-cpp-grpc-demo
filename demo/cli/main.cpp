﻿#include "yutooaisdk.h"
#include <Windows.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <mutex>
#include <vector>

std::vector<std::string> aiResult;

int main() {
    SetConsoleOutputCP(65001);

    while (true) {
        int selectNum = 0;
        std::cout << "---Menu---\n"
                  << "1:Connect\n"
                  << "2:Disconnect\n"
                  << "3:Init\n"
                  << "4:Activate\n"
                  << "5:PluAdd\n"
                  << "6:PluSync\n"
                  << "7:ResetDemo\n"
                  << "8:AiMatching\n"
                  << "9:AiMark\n"
                  << "10:GetCameraList\n"
                  << "11:OpenCamera\n"
                  << "12:GetCameraImage\n"
                  << "13:ShowCameraAreaView\n"
                  << "14:GetCameraArea\n"
                  << "15:SetCameraArea\n"
                  << "16:GetStudyData\n"
                  << "17:SetStudyData\n"
                  << "18:UpdatePluAdd\n"
                  << "19:UpdatePluStatus\n"
                  << "99:Version\n"
                  << "100:Status\n"
                  << "0:Quit\n";
        std::cout << "Select: ";
        std::cin >> selectNum;
        std::cout << "---" << std::endl;
        switch (selectNum) {
        case 1:
            YutooAI_SDK_Connect("127.0.0.1:18101");
            break;
        case 2:
            YutooAI_SDK_Disconnect();
            break;
        case 3: {
            REPLY_STATE state;

            auto ret = YutooAI_SDK_Init(state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 4: {
            REPLY_STATE state;

            std::string key, shopName, contacterName, contacterPhone;
            std::cout << "Key: ";
            std::cin >> key;
            std::cout << "ShopName: ";
            std::cin >> shopName;
            std::cout << "ContacterName: ";
            std::cin >> contacterName;
            std::cout << "ContacterPhone: ";
            std::cin >> contacterPhone;

            auto ret = YutooAI_SDK_Activate(key.c_str(), shopName.c_str(), contacterName.c_str(),
                                            contacterPhone.c_str(), state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 5: {
            YutooAI_SDK_PluAdd("0001", "橘子", true, false);
            YutooAI_SDK_PluAdd("0002", "香梨", true, false);
            YutooAI_SDK_PluAdd("0003", "香蕉", true, false);
            YutooAI_SDK_PluAdd("0004", "苹果", true, false);
            YutooAI_SDK_PluAdd("0005", "哈密瓜", true, false);
            YutooAI_SDK_PluAdd("0006", "西瓜", true, false);
            YutooAI_SDK_PluAdd("1001", "大白菜", true, false);
            YutooAI_SDK_PluAdd("1002", "胡萝卜", true, false);
            YutooAI_SDK_PluAdd("1003", "土豆", true, false);
            YutooAI_SDK_PluAdd("1004", "玉米", true, false);
            YutooAI_SDK_PluAdd("1005", "南瓜", true, false);
            YutooAI_SDK_PluAdd("1006", "番茄", true, false);
        } break;
        case 6: {
            REPLY_STATE state;

            auto ret = YutooAI_SDK_PluSync(state);
            if (ret == 0) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 7: {
            REPLY_STATE state;

            auto ret = YutooAI_SDK_ResetDemo(state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 8: {
            auto ret = YutooAI_SDK_AiMatching(5, [](const REPLY_STATE &replyState, const int &matchindex,
                                                    const long long &identifyId, const int &resultNum,
                                                    const RESULT_INFO *resultList) {
                if (replyState.code == YUTOO_SDK_REPLY_CODE::Success) {
                    std::cout << "Code: " << replyState.code << std::endl;
                    std::cout << "Index: " << matchindex << std::endl;
                    std::cout << "Id: " << identifyId << std::endl;
                    aiResult.clear();
                    for (size_t i = 0; i < resultNum; i++) {
                        std::cout << i << ". " << resultList[i].plucode << ": " << resultList[i].pluname << std::endl;
                        aiResult.push_back(resultList[i].plucode);
                    }
                } else {
                    std::cout << "Failed: \nCode: " << replyState.code << "\nMsg: " << replyState.message << std::endl;
                }
            });
            if (ret == 0) {
                std::cout << "Success" << std::endl;
            }
        } break;
        case 9: {
            REPLY_STATE state;

            long long id;
            std::cout << "Identify Id: ";
            std::cin >> id;

            std::string plu;
            std::cout << "Mark PLU: ";
            std::cin >> plu;

            bool isSearch = true;

            auto p = std::find(aiResult.begin(), aiResult.end(), plu);
            if (p != aiResult.end()) {
                std::cout << "Is Search Mark" << std::endl;
                isSearch = false;
            }

            auto ret = YutooAI_SDK_AiMark(id, plu.c_str(), isSearch, state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 10: {
            ;

            auto ret = YutooAI_SDK_GetCameraList([](const REPLY_STATE &state, const unsigned int &selectedCam,
                                                    const unsigned int &camNum, const CAMERA_INFO *camList) {
                if (state.code == YUTOO_SDK_REPLY_CODE::Success) {
                    std::cout << "Selected Camera: " << selectedCam << std::endl;
                    for (size_t i = 0; i < camNum; i++) {
                        std::cout << camList[i].id << ". " << camList[i].name << std::endl;
                    }
                } else {
                    std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
                }
            });
            if (ret == 0) {
                std::cout << "Success" << std::endl;
            }
        } break;
        case 11: {
            REPLY_STATE state;

            unsigned int id;
            std::cout << "Select Camera Id: ";
            std::cin >> id;

            auto ret = YutooAI_SDK_OpenCamera(id, state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 12: {
            auto ret = YutooAI_SDK_GetCameraImage(
                [](const REPLY_STATE &state, const unsigned char *imageBytes, const int &imageSize) {
                    if (state.code == YUTOO_SDK_REPLY_CODE::Success) {
                        std::cout << imageSize << std::endl;
                    } else {
                        std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
                    }
                });
            if (ret == 0) {
                std::cout << "Success" << std::endl;
            }
        } break;
        case 13: {
            REPLY_STATE state;

            auto ret = YutooAI_SDK_ShowCameraAreaView(state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 14: {
            REPLY_STATE state;
            AREA_INFO   area{0};

            auto ret = YutooAI_SDK_GetCameraArea(area, state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "(" << area.leftTop_x << "," << area.leftTop_y << ")"
                          << ","
                          << "(" << area.rightTop_x << "," << area.rightTop_y << ")"
                          << ","
                          << "(" << area.rightBottom_x << "," << area.rightBottom_y << ")"
                          << ","
                          << "(" << area.leftBottom_x << "," << area.leftBottom_y << ")" << std::endl;
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 15: {
            REPLY_STATE state;
            AREA_INFO   area{0};

            std::cout << "leftTop_x: ";
            std::cin >> area.leftTop_x;
            std::cout << "leftTop_y: ";
            std::cin >> area.leftTop_y;
            std::cout << "rightTop_x: ";
            std::cin >> area.rightTop_x;
            std::cout << "rightTop_y: ";
            std::cin >> area.rightTop_y;
            std::cout << "rightBottom_x: ";
            std::cin >> area.rightBottom_x;
            std::cout << "rightBottom_y: ";
            std::cin >> area.rightBottom_y;
            std::cout << "leftBottom_x: ";
            std::cin >> area.leftBottom_x;
            std::cout << "leftBottom_y: ";
            std::cin >> area.leftBottom_y;

            auto ret = YutooAI_SDK_SetCameraArea(&area, state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 16: {
            YutooAI_SDK_GetStudyData(
                [](const REPLY_STATE &state, const unsigned char *bytes, const int &size, const char *md5) {
                    if (state.code == YUTOO_SDK_REPLY_CODE::Success) {
                        std::cout << "Save to file: data.zip" << std::endl;
                        auto out = std::fstream("data.zip", std::ios::binary | std::ios::out);
                        if (out.is_open()) {
                            out.write((const char *)bytes, size);
                            out.flush();
                            out.close();
                        }
                    } else {
                        std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
                    }
                });
        } break;
        case 17: {
            auto in = std::fstream("data.zip", std::ios::binary | std::ios::in);
            if (in.is_open()) {
                auto begin = in.tellg();
                in.seekg(0, std::ios::end);
                auto datasize = in.tellg() - begin;
                auto datafile = new unsigned char[datasize];
                in.seekg(0, std::ios::beg);
                in.read((char *)datafile, datasize);
                REPLY_STATE state;
                auto        ret = YutooAI_SDK_SetStudyData(datafile, datasize, nullptr, false, state);
                if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                    std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
                } else {
                    std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
                }
                if (datafile) {
                    delete[] datafile;
                }
                in.close();
            }
        } break;
        case 18: {
            YutooAI_SDK_UpdatePluAdd("1001", "测试1", true, true);
            YutooAI_SDK_UpdatePluAdd("1002", "测试2", true, true);
        } break;
        case 19: {
            REPLY_STATE state;

            auto ret = YutooAI_SDK_UpdatePluStatus(state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 99: {
            REPLY_STATE  state;
            VERSION_INFO ver{0};

            auto ret = YutooAI_SDK_Version(ver, state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << ver.sdk << std::endl;
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }
        } break;
        case 100:
            std::cout << "Status Code: " << YutooAI_SDK_Status() << std::endl;
            break;
        case 0: {
            REPLY_STATE state;

            auto ret = YutooAI_SDK_UnInit(state);
            if (ret == 0 && state.code == YUTOO_SDK_REPLY_CODE::Success) {
                std::cout << "Success \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            } else {
                std::cout << "Failed: \nCode: " << state.code << "\nMsg: " << state.message << std::endl;
            }

            YutooAI_SDK_Disconnect();
            goto QUIT;
        } break;
        default:
            std::cout << "Unknown Option: " << selectNum << std::endl;
            break;
        }
        std::cout << std::endl;
    }
QUIT:
    std::cout << "Quit Now" << std::endl;
    return 0;
}
