﻿#pragma once

#ifdef _MSC_VER
#ifdef DLL_EXPORTS
#define API_EXPORT __declspec(dllexport)
#else
#define API_EXPORT __declspec(dllimport)
#endif
#else
#define API_EXPORT __attribute__((visibility("default")))
#endif

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

    /**
     * @brief GRPC状态
     */
    typedef enum GRPC_CHANNEL_STATE : int {
        // 空闲
        CHANNEL_IDLE = 0,
        // 连接中
        CHANNEL_CONNECTING,
        // 已连接
        CHANNEL_READY,
        // 连接出现问题，可能会恢复
        CHANNEL_TRANSIENT_FAILURE,
        // 连接出现问题，无法恢复
        CHANNEL_SHUTDOWN
    } GRPC_CHANNEL_STATE;

    /**
     * @brief SDK响应码
     */
    typedef enum YUTOO_SDK_REPLY_CODE : int {
        /***** 默认配置状态码 *****/
        // 默认的未知错误
        ErrUnknown = 0,
        // 默认的操作成功
        Success    = 1,

        /***** 程序相关状态码 *****/
        // 程序未初始化
        ErrNotInit     = 1001,
        // 程序初始化失败
        ErrInit        = 1002,
        // 接口参数错误
        ErrParams      = 1003,
        // 程序版本信息异常
        ErrVersionInfo = 1004,
        // 修改配置信息失败
        ErrSetConfig   = 1005,
        // 不支持的操作
        ErrNotSupport  = 1006,

        /***** 设备相关状态码 *****/
        // 新旧SN替换错误
        ErrReplaceSN   = 2001,
        // 设备未激活
        ErrNotActivate = 2002,
        // 设备激活失败
        ErrActivate    = 2003,
        // 设备信息异常
        ErrDeviceInfo  = 2004,

        /***** SO库相关状态码 *****/
        // SO库未加载
        ErrNotLoadSo   = 3001,
        // SO库加载失败
        ErrLoadSo      = 3002,
        // SO库未初始化
        ErrNotInitSo   = 3003,
        // SO库初始化失败
        ErrInitSo      = 3004,
        // 识别异常
        ErrAi          = 3005,
        // 无识别记录
        ErrNotIdentify = 3006,

        /***** 相机相关状态码 *****/
        // 相机异常
        ErrCamera = 4001,
        // 图像处理异常
        ErrImage  = 4002,

        /***** 商品相关状态码 *****/
        // 初始化商品数据异常
        ErrInitPluData   = 5001,
        // 初始化ICON匹配服务异常
        ErrInitIconMatch = 5002,
        // 商品ICON匹配异常
        ErrIconMatch     = 5003,
        // 商品不存在
        ErrPluNotExist   = 5004,
        // 商品已锁定
        ErrPluLock       = 5005,
        // 商品信息修改失败
        ErrPluSetInfo    = 5006,

        /***** 网络相关状态码 *****/
        // 网络异常
        ErrNetwork = 6001,
    } YUTOO_SDK_REPLY_CODE;

    /**
     * @brief 响应状态
     */
    typedef struct REPLY_STATE {
        YUTOO_SDK_REPLY_CODE code;
        char                 message[256] = {0};
    } REPLY_STATE;

    /**
     * @brief 商品信息
     */
    typedef struct RESULT_INFO {
        const char *plucode;
        const char *pluname;
    } RESULT_INFO;

    /**
     * @brief 相机信息
     */
    typedef struct CAMERA_INFO {
        const unsigned int id;
        const char        *name;
        const char        *symbol;
        const char        *location;
    } CAMERA_INFO;

    /**
     * @brief 区域信息
     */
    typedef struct AREA_INFO {
        int leftTop_x;
        int leftTop_y;
        int rightTop_x;
        int rightTop_y;
        int rightBottom_x;
        int rightBottom_y;
        int leftBottom_x;
        int leftBottom_y;
    } AREA_INFO;

    /**
     * @brief 版本信息
     */
    typedef struct VERSION_INFO {
        const char *sdk;
        const char *camera;
        const char *identify;
        const char *matching;
        const char *machine;
    } VERSION_INFO;

    /**
     * @brief 连接至SDK
     * @param addr SDK监听地址
     * @attention 结束程序前请务必调用 @ref YutooAI_SDK_Disconnect()
     */
    API_EXPORT void YutooAI_SDK_Connect(const char *addr);

    /**
     * @brief 断开SDK连接
     */
    API_EXPORT void YutooAI_SDK_Disconnect();

    /**
     * @brief 获取GRPC连接状态
     * @return GRPC状态
     */
    API_EXPORT GRPC_CHANNEL_STATE YutooAI_SDK_Status();

    /**
     * @brief 初始化
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_Init(REPLY_STATE &state);

    /**
     * @brief 反初始化
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_UnInit(REPLY_STATE &state);

    /**
     * @brief 激活
     * @param key 激活码
     * @param shopName 商店名称
     * @param contacterName 联系人名称
     * @param contacterPhone 联系人电话
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_Activate(const char *key, const char *shopName, const char *contacterName,
                                        const char *contacterPhone, REPLY_STATE &state);

    /**
     * @brief 添加商品至缓存列表
     * @param plucode 商品PLU
     * @param pluname 商品名称
     * @param sale 是否销售
     * @param lock 是否锁定
     * @attention 调用 @ref YutooAI_SDK_PluSync() 同步缓存列表
     */
    API_EXPORT void YutooAI_SDK_PluAdd(const char *plucode, const char *pluname, bool sale, bool lock);

    /**
     * @brief 同步商品缓存列表至SDK
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_PluSync(REPLY_STATE &state);

    /**
     * @brief 添加商品状态至缓存列表
     * @param plucode 商品PLU
     * @param pluname 商品名称
     * @param sale 是否销售
     * @param lock 是否锁定
     * @attention 调用 @ref YutooAI_SDK_UpdatePluStatus() 同步缓存列表
     */
    API_EXPORT void YutooAI_SDK_UpdatePluAdd(const char *plucode, const char *pluname, bool sale, bool lock);

    /**
     * @brief 更新缓存列表商品状态
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_UpdatePluStatus(REPLY_STATE &state);

    /**
     * @brief 重置为演示模式
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_ResetDemo(REPLY_STATE &state);

    /**
     * @brief 识别结果回调
     * @param matchindex 识别次数
     * @param identifyId 识别ID
     * @param resultNum 识别结果数量
     * @param resultList 识别结果列表
     */
    typedef void(YutooAI_SDK_AiMatching_CallBack)(const REPLY_STATE &state, const int &matchIndex,
                                                  const long long &identifyId, const int &resultNum,
                                                  const RESULT_INFO *resultList);

    /**
     * @brief 识别
     * @param maxNum 最大识别输出数量
     * @param cb 识别回调函数
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_AiMatching(int maxNum, YutooAI_SDK_AiMatching_CallBack cb);

    /**
     * @brief 标记
     * @param plucode 商品PLU
     * @param isSearch 是否为搜索标记
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_AiMark(const long long identifyid, const char *plucode, bool isSearch,
                                      REPLY_STATE &state);

    /**
     * @brief 图像回调
     * @param size 数据大小
     * @param bytes 数据指针
     */
    typedef void(YutooAI_SDK_Image_CallBack)(const REPLY_STATE &state, const unsigned char *imageBytes,
                                             const int &imageSize);

    /**
     * @brief 学习模式识别
     * @param cb 相机图像回调函数
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_StudyMatching(YutooAI_SDK_Image_CallBack cb);

    /**
     * @brief 学习模式标记
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_StudyMark(REPLY_STATE &state);

    /**
     * @brief 相机列表回调
     */
    typedef void(YutooAI_SDK_CameraList_CallBack)(const REPLY_STATE &state, const unsigned int &selectedCam,
                                                  const unsigned int &camNum, const CAMERA_INFO *camList);

    /**
     * @brief 获取相机列表
     * @param cb 相机列表同步回调
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_GetCameraList(YutooAI_SDK_CameraList_CallBack cb);

    /**
     * @brief 打开指定相机
     * @param id 相机id
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_OpenCamera(unsigned int id, REPLY_STATE &state);

    /**
     * @brief 获取相机图像
     * @param cb 相机图像回调函数
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_GetCameraImage(YutooAI_SDK_Image_CallBack cb);

    /**
     * @brief 获取秤盘区域
     * @param area 秤盘区域
     * @param state 请求返状态
     * @return 连接状态
     * @attention 坐标为顺时针顺序
     */
    API_EXPORT int YutooAI_SDK_GetCameraArea(AREA_INFO &area, REPLY_STATE &state);

    /**
     * @brief 设置秤盘区域
     * @param area 秤盘区域
     * @param state 请求返状态
     * @return 连接状态
     * @attention 坐标为顺时针顺序
     */
    API_EXPORT int YutooAI_SDK_SetCameraArea(AREA_INFO *area, REPLY_STATE &state);

    /**
     * @brief 显示区域设置页面
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_ShowCameraAreaView(REPLY_STATE &state);

    /**
     * @brief 数据回调
     * @param size 数据大小
     * @param bytes 数据指针
     */
    typedef void(YutooAI_SDK_Bytes_CallBack)(const REPLY_STATE &state, const unsigned char *bytes, const int &size,
                                             const char *md5);

    /**
     * @brief 获取学习数据
     * @param cb 学习数据回调
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_GetStudyData(YutooAI_SDK_Bytes_CallBack cb);

    /**
     * @brief 设置学习数据
     * @param size 数据大小
     * @param bytes 数据指针
     * @param md5   数据md5校验(空不校验)
     * @param merge 是否追加数据
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_SetStudyData(const unsigned char *bytes, const int &size, const char *md5, bool merge,
                                            REPLY_STATE &state);

    /**
     * @brief 版本相关信息
     * @param ver 返回版本信息
     * @param state 请求返状态
     * @return 连接状态
     */
    API_EXPORT int YutooAI_SDK_Version(VERSION_INFO &ver, REPLY_STATE &state);

#ifdef __cplusplus
}
#endif
