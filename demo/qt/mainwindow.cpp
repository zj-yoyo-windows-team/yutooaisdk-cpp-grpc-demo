﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    ui->statusBar->showMessage("Not Connect");
    // QStringList arg = QStringList() << "-c"
    //                                << "<Vendor>"
    //                                << "-k"
    //                                << "<Key>"
    //                                << "-p"
    //                                << "18101";
    // yutooProces = std::make_unique<QProcess>(this);
    // if (yutooProces) {
    //    yutooProces->setWorkingDirectory("YuTooAISDK\\");
    //    yutooProces->setProgram("YuTooAISDK\\yutoo_ai_x86.exe");
    //    yutooProces->setArguments(arg);
    //    yutooProces->start();
    //}
    connect(this, SIGNAL(update_text_signal(QString)), this, SLOT(UpdateText(QString)));
    connect(this, SIGNAL(update_status_bar_signal()), this, SLOT(UpdateStatusBar()));

    camWindow = new CameraWindow(this);
    camWindow->setWindowFlags(Qt::Window);
}

MainWindow::~MainWindow() {
    // if (yutooProces) {
    //    yutooProces->kill();
    //    yutooProces->disconnect();
    //    yutooProces->waitForFinished(1000);
    //}
    delete camWindow;
    //    delete camUi;
    delete ui;
}

void MainWindow::on_actionExit_triggered() { this->close(); }

void MainWindow::UpdateText(QString str) {
    ui->textEdit->append(QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz") + "\n" + str + "\n");
    ui->textEdit->moveCursor(QTextCursor::End);
}

void MainWindow::UpdateStatusBar() {
    if (yutooAi) {
        switch (yutooAi->GetChannelStatus()) {
        case grpc_connectivity_state::GRPC_CHANNEL_CONNECTING:
            ui->statusBar->showMessage("Connecting");
            break;
        case grpc_connectivity_state::GRPC_CHANNEL_IDLE:
            ui->statusBar->showMessage("Idel");
            break;
        case grpc_connectivity_state::GRPC_CHANNEL_READY:
            ui->statusBar->showMessage("Ready");
            break;
        case grpc_connectivity_state::GRPC_CHANNEL_SHUTDOWN:
            ui->statusBar->showMessage("Shutdown");
            break;
        case grpc_connectivity_state::GRPC_CHANNEL_TRANSIENT_FAILURE:
            ui->statusBar->showMessage("Failure");
            break;
        default:
            ui->statusBar->showMessage("Unknown");
            break;
        }
    } else {
        ui->statusBar->showMessage("Not Connect");
    }
}

void MainWindow::on_connectBtn_clicked() {
    if (yutooAi) {
        yutooAi->GetChannelStatus(true);
    } else {
        yutooAi = std::make_unique<YutooAiClient>(ui->ipAddrEdit->text().toStdString());
    }
    emit update_status_bar_signal();
}

void MainWindow::on_disconnectBtn_clicked() {
    yutooAi.reset();
    emit update_status_bar_signal();
}

void MainWindow::on_getVersionBtn_clicked() {
    if (yutooAi) {
        GetVersionReply reply;
        Status          stat = yutooAi->GetVersion(&reply);
        if (stat.ok()) {
            auto result =
                QString("GetVersion:\n%1(%2) %3\nsdk: %4\ncamera: %5\nidentify: %6\nmatching: %7\nmachine: %8")
                    .arg(ReplyCode_Name(reply.status().code()).c_str())
                    .arg(reply.status().code())
                    .arg(reply.status().msg().c_str(), reply.sdkversion().c_str(), reply.cameraversion().c_str(),
                         reply.identifyversion().c_str(), reply.matchingversion().c_str(),
                         reply.machineversion().c_str());

            emit update_text_signal(result);
        } else {
            emit update_text_signal(QString("Error %1: %2").arg(stat.error_code()).arg(stat.error_message().c_str()));
        }
    }
    emit update_status_bar_signal();
}

void MainWindow::on_initBtn_clicked() {
    if (yutooAi) {
        PublicReply reply;
        Status      stat = yutooAi->Init(&reply);
        if (stat.ok()) {
            emit update_text_signal(QString("Init:\n%1(%2) %3")
                                        .arg(ReplyCode_Name(reply.code()).c_str())
                                        .arg(reply.code())
                                        .arg(reply.msg().c_str()));
        } else {
            emit update_text_signal(QString("Error %1: %2").arg(stat.error_code()).arg(stat.error_message().c_str()));
        }
    }
    emit update_status_bar_signal();
}

void MainWindow::on_unInitBtn_clicked() {
    if (yutooAi) {
        PublicReply reply;
        Status      stat = yutooAi->Init(&reply);
        if (stat.ok()) {
            emit update_text_signal(QString("UnInit:\n%1(%2) %3")
                                        .arg(ReplyCode_Name(reply.code()).c_str())
                                        .arg(reply.code())
                                        .arg(reply.msg().c_str()));
        } else {
            emit update_text_signal(QString("Error %1: %2").arg(stat.error_code()).arg(stat.error_message().c_str()));
        }
    }
    emit update_status_bar_signal();
}

void MainWindow::on_activateBtn_clicked() {
    QDialog dialog(this);
    dialog.setWindowTitle("Activate");

    QFormLayout form(&dialog);
    form.addRow(new QLabel("Please Input Activate Information:"));

    QLineEdit *activateCode = new QLineEdit(&dialog);
    form.addRow("ActivateCode", activateCode);

    QLineEdit *shopName = new QLineEdit(&dialog);
    form.addRow("ShopName", shopName);

    QLineEdit *contacterName = new QLineEdit(&dialog);
    form.addRow("ContacterName", contacterName);

    QLineEdit *contacterPhone = new QLineEdit(&dialog);
    form.addRow("ContacterPhone", contacterPhone);

    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    QObject::connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() == QDialog::Accepted) {
        ActivateRequest request;
        request.set_cdkey(activateCode->text().toStdString());
        request.set_shopname(shopName->text().toStdString());
        request.set_shopcontact(contacterName->text().toStdString());
        request.set_contactphone(contacterPhone->text().toStdString());

        PublicReply reply;
        if (yutooAi) {
            Status stat = yutooAi->Activate(request, &reply);
            if (stat.ok()) {
                emit update_text_signal(QString("Activate:\n%1(%2) %3")
                                            .arg(ReplyCode_Name(reply.code()).c_str())
                                            .arg(reply.code())
                                            .arg(reply.msg().c_str()));
            } else {
                emit update_text_signal(
                    QString("Error %1: %2").arg(stat.error_code()).arg(stat.error_message().c_str()));
            }
        }
    }
    emit update_status_bar_signal();
}

void MainWindow::on_pluSyncBtn_clicked() {
    std::string basename = "Item";
    if (yutooAi) {
        PluSyncRequest request;
        request.set_resetdemo(false);
        for (int i = 1000; i < 1100; i++) {
            auto newitem = request.add_plulist();
            newitem->set_plucode(std::to_string(i));
            newitem->set_pluname(basename + std::to_string(i));
            newitem->set_ison(true);
            newitem->set_islock(false);
        }

        PublicReply reply;
        Status      stat = yutooAi->PluSync(request, &reply);
        if (stat.ok()) {
            emit update_text_signal(QString("PluSync:\n%1(%2) %3")
                                        .arg(ReplyCode_Name(reply.code()).c_str())
                                        .arg(reply.code())
                                        .arg(reply.msg().c_str()));
        } else {
            emit update_text_signal(QString("Error %1: %2").arg(stat.error_code()).arg(stat.error_message().c_str()));
        }
    }
    emit update_status_bar_signal();
}

// 该回调为异步回调，注意线程安全
void MainWindow::ai_matching_callback(const Status stat, const AiMatchingReply reply) {
    if (stat.ok()) {

        auto result = QString("AiMatching:\n%1(%2) %3\nweight: %4 maxnum: %5\ntype: %6\nindex: %7\nid: %8\nresult:")
                          .arg(ReplyCode_Name(reply.status().code()).c_str())
                          .arg(reply.status().code())
                          .arg(reply.status().msg().c_str())
                          .arg(reply.aimatchingrequest().weight())
                          .arg(reply.aimatchingrequest().maxnum())
                          .arg(MatchingType_Name(reply.aimatchingrequest().matchtype()).c_str())
                          .arg(reply.matchindex())
                          .arg(reply.identifyid());
        auto list = reply.plulist();
        for (auto itr = list.begin(); itr != list.end(); ++itr) {
            auto plu = QString("\n%1: %2").arg(itr->plucode().c_str(), itr->pluname().c_str());
            result.append(plu);
        }

        emit update_text_signal(result);
    } else {
        emit update_text_signal(QString("Error %1: %2").arg(stat.error_code()).arg(stat.error_message().c_str()));
    }
}

void MainWindow::on_aiMatchingBtn_clicked() {
    if (yutooAi) {
        AiMatchingRequest request;
        request.set_weight(100);
        request.set_stable(WeightStatus::weightNotStable);
        request.set_maxnum(5);
        request.set_matchtype(MatchingType::auto_);

        yutooAi->AiMatching(
            request, [this](const Status s, const AiMatchingReply r) { return this->ai_matching_callback(s, r); });
        Sleep(100);

        auto reply_call =
            std::bind(&MainWindow::ai_matching_callback, this, std::placeholders::_1, std::placeholders::_2);

        AiMatchingRequest request2;
        request2.set_weight(140);
        request2.set_stable(WeightStatus::weightNotStable);
        request2.set_maxnum(5);
        request2.set_matchtype(MatchingType::auto_);
        yutooAi->AiMatching(request2, reply_call);
        //        Sleep(100);

        AiMatchingRequest request3;
        request3.set_weight(150);
        request3.set_stable(WeightStatus::weightStable);
        request3.set_maxnum(5);
        request3.set_matchtype(MatchingType::auto_);
        yutooAi->AiMatching(request3, reply_call);
        //        Sleep(100);

        AiMatchingRequest request4;
        request4.set_weight(150);
        request4.set_stable(WeightStatus::weightStable);
        request4.set_maxnum(5);
        request4.set_matchtype(MatchingType::forcibly);
        yutooAi->AiMatching(request4, reply_call);
        //        Sleep(100);

        AiMatchingRequest request5;
        request5.set_weight(0);
        request5.set_stable(WeightStatus::weightStable);
        request5.set_maxnum(5);
        request5.set_matchtype(MatchingType::auto_);
        yutooAi->AiMatching(request5, reply_call);
    }
    emit update_status_bar_signal();
}

void MainWindow::on_aiMarkBtn_clicked() {
    bool    is_mark = false;
    QString mark_plu =
        QInputDialog::getText(this, "Mark", "Please Input PLU to Mark:", QLineEdit::Normal, "", &is_mark);
    if (is_mark && !mark_plu.isEmpty()) {
        AiMarkRequest request;
        request.set_marktype(MarkType::matchOut);
        request.set_plucode(mark_plu.toStdString());
        request.set_sales(100);
        request.set_priceunit(PriceUnitType::WeightType);
        request.set_price(100);
        request.set_saleprice(100);
        request.set_amount(10000);
        request.set_barcode(mark_plu.toStdString());

        PublicReply reply;
        if (yutooAi) {
            Status stat = yutooAi->AiMark(request, &reply);
            if (stat.ok()) {
                emit update_text_signal(QString("Activate:\n%1(%2) %3")
                                            .arg(ReplyCode_Name(reply.code()).c_str())
                                            .arg(reply.code())
                                            .arg(reply.msg().c_str()));
            } else {
                emit update_text_signal(
                    QString("Error %1: %2").arg(stat.error_code()).arg(stat.error_message().c_str()));
            }
        }
    }
    emit update_status_bar_signal();
}

void MainWindow::on_actionReset_triggered() {
    if (yutooAi) {
        PluSyncRequest request;
        request.set_resetdemo(true);
        request.add_plulist();

        PublicReply reply;
        Status      stat = yutooAi->PluSync(request, &reply);
        if (stat.ok()) {
            emit update_text_signal(QString("ResetDemo:\n%1(%2) %3")
                                        .arg(ReplyCode_Name(reply.code()).c_str())
                                        .arg(reply.code())
                                        .arg(reply.msg().c_str()));
        } else {
            emit update_text_signal(QString("Error %1: %2").arg(stat.error_code()).arg(stat.error_message().c_str()));
        }
    }
    emit update_status_bar_signal();
}

void MainWindow::on_cameraStreamBtn_clicked() {
    if (yutooAi) {
        camWindow->show();
        GetCameraStreamRequest req;
        req.set_fps(15);
        auto reply_call = std::bind(&CameraWindow::UpdateCall, camWindow, std::placeholders::_1);
        auto stop_call  = std::bind(&YutooAiClient::StopCameraStream, yutooAi.get());
        yutooAi->StartCameraStream(req, reply_call);
        camWindow->SetStopEvent(stop_call);
    }
    emit update_status_bar_signal();
}
