﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDateTime>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QInputDialog>
#include <QLabel>
#include <QMainWindow>
#include <QMessageBox>
#include <QProcess>

#include <yutooai.hpp>

#include "camerawindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

  public slots:
    void UpdateText(QString str);
    void UpdateStatusBar();

  private:
    void ai_matching_callback(const Status stat, const AiMatchingReply reply);
  signals:
    void update_text_signal(QString str);
  signals:
    void update_status_bar_signal();

  private slots:
    void on_actionExit_triggered();

    void on_connectBtn_clicked();

    void on_disconnectBtn_clicked();

    void on_getVersionBtn_clicked();

    void on_initBtn_clicked();

    void on_activateBtn_clicked();

    void on_aiMatchingBtn_clicked();

    void on_unInitBtn_clicked();

    void on_actionReset_triggered();

    void on_pluSyncBtn_clicked();

    void on_aiMarkBtn_clicked();

    void on_cameraStreamBtn_clicked();

  private:
    Ui::MainWindow *               ui;
    CameraWindow *                 camWindow;
    // std::unique_ptr<QProcess>      yutooProces;
    std::unique_ptr<YutooAiClient> yutooAi;
};

#endif // MAINWINDOW_H
