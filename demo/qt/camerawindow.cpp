#include "camerawindow.h"
#include "ui_camerawindow.h"

#include <QCloseEvent>

CameraWindow::CameraWindow(QWidget *parent) : QWidget(parent), ui(new Ui::CameraWindow) {
    ui->setupUi(this);
    ui->imgLabel->setMinimumSize(400, 300);
}

CameraWindow::~CameraWindow() { delete ui; }

void CameraWindow::closeEvent(QCloseEvent *event) {
    if (stopEvent) {
        stopEvent();
    }
    this->hide();
    event->ignore();
}

void CameraWindow::resizeEvent(QResizeEvent *event) {
    updateFrame();
    QWidget::resizeEvent(event);
}

void CameraWindow::updateFrame() {
    if (!image.isNull()) {
        ui->imgLabel->setPixmap(image.scaled(ui->imgLabel->size(), Qt::KeepAspectRatio));
    }
}

void CameraWindow::UpdateCall(const GetCameraStreamReply reply) {
    if (reply.status().code() == ReplyCode::Success) {
        auto bytes = reply.stream();
        SetImage((uchar *)bytes.data(), bytes.length());
        updateFrame();
    }
    //    if(!this->isVisible()){
    //        if(stopEvent){
    //            stopEvent();
    //        }
    //    }
}

void CameraWindow::SetStopEvent(StopEventCallback event) { stopEvent = event; }

void CameraWindow::SetImage(const uchar *buff, uint size) { image.loadFromData(buff, size); }

void CameraWindow::SetImage(QPixmap &img) { image = img.copy(); }
