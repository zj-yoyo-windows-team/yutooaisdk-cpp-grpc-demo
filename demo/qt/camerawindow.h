#ifndef CAMERAWINDOW_H
#define CAMERAWINDOW_H

#include <QLabel>
#include <QWidget>

#include <yutooai.hpp>

namespace Ui {
class CameraWindow;
}

class CameraWindow : public QWidget {
    Q_OBJECT

  public:
    typedef std::function<void()> StopEventCallback;
    explicit CameraWindow(QWidget *parent = nullptr);
    ~CameraWindow();
    void UpdateCall(const GetCameraStreamReply reply);
    void SetStopEvent(std::function<void(void)>);
    void SetImage(const uchar *buff, uint size);
    void SetImage(QPixmap &img);

  protected:
    void updateFrame();
    void closeEvent(QCloseEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

  private:
    Ui::CameraWindow *ui;
    QPixmap           image;
    StopEventCallback stopEvent;
};

#endif // CAMERAWINDOW_H
