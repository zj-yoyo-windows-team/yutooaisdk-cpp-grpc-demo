﻿#pragma once

#include <condition_variable>
#include <shared_mutex>
#include <thread>

#ifndef NDEBUG
#ifdef _WIN32
#include <Windows.h>
static void WINAPIV DebugOut(const char *fmt, ...) {
    char    s[1025];
    va_list args;
    va_start(args, fmt);
    wvsprintfA(s, fmt, args);
    va_end(args);
    OutputDebugStringA(s);
}
#define LOGD(...) DebugOut(__VA_ARGS__)
#else
#define LOGD(...) printf(__VA_ARGS__)
#endif
#else
#define LOGD(...)
#endif

#include "client/PrivateInterface.grpc.pb.h"
#include "client/PublicInterface.grpc.pb.h"
#include <grpcpp/grpcpp.h>

using namespace grpc;
using namespace YoYo::YuTooAiPb::V1;

class YutooAiClient {
  private:
    std::shared_ptr<Channel>            channel;
    std::shared_ptr<YoYoAiPublic::Stub> stub;

  public:
    YutooAiClient(std::string ip) {
        ChannelArguments option;
        option.SetMaxReceiveMessageSize(128 * 1024 * 1024);
        option.SetMaxSendMessageSize(128 * 1024 * 1024);
        channel = grpc::CreateCustomChannel(ip, grpc::InsecureChannelCredentials(), option);

        stub = YoYoAiPublic::NewStub(channel);
    }

    ~YutooAiClient() {
        stub.reset();
        channel.reset();
    }

    std::shared_ptr<YoYoAiPublic::Stub> Stub() { return stub; }

    grpc_connectivity_state GetChannelStatus(bool reconnect = false) { return channel->GetState(reconnect); }

    Status Init(PublicReply &reply) {
        ClientContext ctx;
        EmptyRequest  req;

        return stub->Init(&ctx, req, &reply);
    }

    Status UnInit(PublicReply &reply) {
        ClientContext ctx;
        EmptyRequest  req;

        return stub->UnInit(&ctx, req, &reply);
    }

    Status Activate(ActivateRequest &request, PublicReply &reply) {
        ClientContext ctx;

        return stub->Activate(&ctx, request, &reply);
    }

    Status PluSync(PluSyncRequest &request, PublicReply &reply) {
        ClientContext ctx;

        return stub->PluSync(&ctx, request, &reply);
    }

    Status UpdatePluStatus(UpdatePluStatusRequest &request, PublicReply &reply) {
        ClientContext ctx;

        return stub->UpdatePluStatus(&ctx, request, &reply);
    }

    Status AiMatching(AiMatchingRequest &request, AiMatchingReply &reply) {
        ClientContext ctx;

        return stub->AiMatching(&ctx, request, &reply);
    }

    Status AiMark(AiMarkRequest &request, PublicReply &reply) {
        ClientContext ctx;

        return stub->AiMark(&ctx, request, &reply);
    }

    Status StudyModeMatching(StudyModeMatchingReply &reply) {
        ClientContext ctx;
        EmptyRequest  req;

        return stub->StudyModeMatching(&ctx, req, &reply);
    }

    Status StudyModelMark(StudyModeMarkRequest &request, PublicReply &reply) {
        ClientContext ctx;

        return stub->StudyModeMark(&ctx, request, &reply);
    }

    Status GetCameraImage(GetCameraStreamReply &reply) {
        ClientContext ctx;
        EmptyRequest  req;

        return stub->GetCameraImage(&ctx, req, &reply);
    }

    Status GetCameraList(GetCameraListReply &reply) {
        ClientContext ctx;
        EmptyRequest  req;

        return stub->GetCameraList(&ctx, req, &reply);
    }

    Status OpenCamera(OpenCameraRequest &request, PublicReply &reply) {
        ClientContext ctx;

        return stub->OpenCamera(&ctx, request, &reply);
    }

    Status GetCameraArea(GetCameraAreaReply &reply) {
        ClientContext ctx;
        EmptyRequest  req;

        return stub->GetCameraArea(&ctx, req, &reply);
    }

    Status SetCameraArea(SetCameraAreaRequest &request, PublicReply &reply) {
        ClientContext ctx;

        return stub->SetCameraArea(&ctx, request, &reply);
    }

    Status ShowCameraAreaView(PublicReply &reply) {
        ClientContext ctx;
        EmptyRequest  req;

        return stub->ShowCameraAreaView(&ctx, req, &reply);
    }

    Status GetStudyData(GetStudyDataReply &reply) {
        ClientContext ctx;
        EmptyRequest  req;

        return stub->GetStudyData(&ctx, req, &reply);
    }

    Status SetStudyData(SetStudyDataRequest &request, PublicReply &reply) {
        ClientContext ctx;

        return stub->SetStudyData(&ctx, request, &reply);
    }

    Status GetVersion(GetVersionReply &reply) {
        ClientContext ctx;
        EmptyRequest  req;

        return stub->GetVersion(&ctx, req, &reply);
    }
};
